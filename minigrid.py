import gym
import gym_minigrid
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions.categorical import Categorical

# A simple, memoryless MLP agent. Last layer are logits (scores for
# which higher values represent preferred actions.
class Policy(nn.Module):
    def __init__(self, obs_size, act_size, inner_size, **kwargs):
        super(Policy, self).__init__(**kwargs)
        self.affine1 = nn.Linear(obs_size, inner_size)
        self.affine2 = nn.Linear(inner_size, act_size)

        self.saved_log_probs = []
        self.rewards = []

    def forward(self, x):
        x = x.view(-1, 7*7)
        x = F.relu(self.affine1(x))
        act_probs = self.affine2(x)
        return act_probs

# Function that, given a policy network and a state selects a random
# action according to the probabilities output by final layer.
def select_action(policy, state, deterministic=False):
    probs = policy(state)
    dist = Categorical(logits=probs)
    if not deterministic:
        action = dist.sample()
    else:
        action = torch.tensor([torch.argmax(probs).item()])
    return (action, dist.log_prob(action))
        
# Utility function. The MiniGrid gym environment uses 3 channels as
# state, but for this we only use the first channel: represents all
# objects (including goal) with integers. This function just strips
# out the first channel and returns it.
def state_filter(state):
    return torch.from_numpy(state['image'][:,:,0]).float()

# Function to compute discounted rewards after a complete episode.
def compute_discounted_rewards(rewards, gamma=0.99):
    discounted_rewards = []
    running = 0.0
    for r in reversed(rewards):
        running = r + gamma * running
        discounted_rewards.append(running)
    return list(reversed(discounted_rewards))

# The function that runs the simulation for a specified length. The
# nice thing about the MiniGrid environment is that the game never
# ends. After achieving the goal, the game resets. Kind of like
# Sisyphus...
def run_episode(env, policy, length, gamma=0.99, render=False, deterministic=False):
    # Restart the MiniGrid environment.
    state = state_filter(env.reset())

    # We need to keep a record of states, actions, and the
    # instantaneous rewards.
    states = [state]
    actions = []
    rewards = []
    logprobs = []
    
    # Run for desired episode length.
    for _ in range(length):
        # Get action from policy net based on current state.
        (action, lp) = select_action(policy, state, deterministic=deterministic)

        # MiniGrid has a QT5 renderer which is pretty cool.
        if render:
            env.render()

        # Simulate one step, get new state and instantaneous reward.
        state, reward, done, _ = env.step(action)
        state = state_filter(state)
        states.append(state)
        rewards.append(reward)
        actions.append(action)
        logprobs.append(lp)
        if done:
            break

    # Finished with episode, compute loss per step.
    discounted_rewards = compute_discounted_rewards(rewards, gamma)

    # Return the sequence of states, actions, and the corresponding rewards.
    return (states, actions, logprobs, discounted_rewards)

class EmptyRandomEnv8x8(gym_minigrid.envs.empty.EmptyEnv):
    def __init__(self):
        super().__init__(size=8, agent_start_pos=None)
        
###### The main loop.
if __name__ == '__main__':
    ###### Some configuration variables.
    episode_len = 50  # Length of each game.
    obs_size = 7*7    # MiniGrid uses a 7x7 window of visibility.
    act_size = 7      # Seven possible actions (turn left, right, forward, pickup, drop, etc.)
    inner_size = 128  # Number of neurons in two hidden layers.
    lr = 0.001        # Adam learning rate
    avg_reward = 0.0  # For tracking average regard per episode.

    # Setup OpenAI Gym environment for guessing game.
    env = gym.make('MiniGrid-Empty-6x6-v0')

    # Instantiate a policy network.
    policy = Policy(obs_size=obs_size, act_size=act_size, inner_size=inner_size)

    # Use the Adam optimizer.
    optimizer = torch.optim.Adam(params=policy.parameters(), lr=lr)

    # Run for a while.
    episodes = 20000
    average_rewards = []
    for step in range(episodes):
        # Run an episode.
        (states, actions, logprobs, discounted_rewards) = run_episode(env,
                                                                      policy,
                                                                      episode_len,
                                                                      gamma=0.99,
                                                                      render=step % 100 == 0)
        avg_reward += discounted_rewards[0]
        if step % 100 == 0:
            print(f'Average reward @ episode {step}: {avg_reward / 100}')
            average_rewards.append(avg_reward / 100)
            avg_reward = 0.0
        
        # Repeat each action, and backpropagate discounted
        # rewards. This can probably be batched for efficiency with a
        # memoryless agent...
        optimizer.zero_grad()
        loss = -torch.sum(torch.tensor(discounted_rewards) * torch.stack(logprobs))
        loss.backward()
        optimizer.step()

    # Plot average reward per episode
    plt.figure()
    plt.plot(average_rewards)
    plt.ylabel('Average reward')
    plt.xlabel('Episodes / 100')
    plt.show()

    # Animate some runs with the final trained agent.
    for step in range(50):
        run_episode(env, policy, episode_len, gamma=0.99, render=True)
    
        
