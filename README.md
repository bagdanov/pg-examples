# Simple Policy Gradient Examples

This repository contains two simple examples of using *policy
gradient* (REINFORCE, basically) to solve reinforcement learning problems.

## Getting started

These example scripts have the following dependencies:
+ pytorch
+ matplotlib
+ gym[classic-control]
+ gym-minigrid

If you are using [Anaconda](https://www.anaconda.com/products/distribution), you should be able to create an environment with all dependencies with:
```
conda env create --name pg-examples -f environment.yml
```
from the command line. After the environment is created, you should be able to activate it and run the scripts. For example:
```
conda activate pg-examples
python cartpole.py
```

**Note**: The Open Gym environments can be a bit *finicky*. You might have to fiddle with dependencies to get some things working.
