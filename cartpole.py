import matplotlib.pyplot as plt
import gym
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions.categorical import Categorical

# A simple, memoryless MLP agent. Last layer are logits (scores for
# which higher values represent preferred actions.
class Policy(nn.Module):
    def __init__(self, obs_size, act_size, inner_size, **kwargs):
        super(Policy, self).__init__(**kwargs)
        self.affine1 = nn.Linear(obs_size, inner_size)
        self.affine2 = nn.Linear(inner_size, act_size)
        self.rewards = []

    def forward(self, x):
        x = F.relu(self.affine1(x))
        act_logits = self.affine2(x).clamp(-1000.0, +1000.0)
        return act_logits

# Function that, given a policy network and a state selects a random
# action according to the probabilities output by final layer. Returns
# the selected action and the log probability of that choice.
def select_action(policy, state):
    probs = policy.forward(torch.from_numpy(state))
    dist = Categorical(logits=probs)
    action = dist.sample()
    return (action.item(), dist.log_prob(action))

# Function to compute discounted rewards after a complete episode.
def compute_discounted_rewards(rewards, gamma=0.99):
    discounted_rewards = []
    running = 0.0
    for r in reversed(rewards):
        running = r + gamma * running
        discounted_rewards.append(running)
    return list(reversed(discounted_rewards))

# The function that runs the simulation for a specified
# length. Actually, for Cartpole episodes last for a maximum of 500
# steps.
def run_episode(env, policy, length, gamma=0.99, render=False):
    # Restart the environment.
    state = env.reset()

    # We need to keep a record of states, actions, and the
    # instantaneous rewards.
    states = [state]
    actions = []
    rewards = []
    logprobs = []
    
    # Run for desired episode length.
    for _ in range(length):
        # Get action from policy net based on current state.
        (action, logprob) = select_action(policy, state)

        # Render if desired.
        if render:
            env.render()

        # Simulate one step, get new state and instantaneous reward.
        state, reward, done, _ = env.step(action)
        states.append(torch.tensor(state))
        rewards.append(reward)
        actions.append(action)
        logprobs.append(logprob)
        if done:
            break

    # Finished with episode, compute return per step.
    discounted_rewards = compute_discounted_rewards(rewards, gamma)

    # Return the sequence of states, actions, and the corresponding rewards.
    return (states, actions, logprobs, discounted_rewards)

###### The main loop.
if __name__ == '__main__':
    ###### Some configuration variables.
    episode_len = 500  # Length of each game.
    obs_size = 4       # Size of the CartPole observation space.
    act_size = 2       # Two actions: accellerate left or right.
    inner_size = 64    # Number of neurons in two hidden layers.
    lr = 0.001         # Adam learning rate

    # Setup OpenAI Gym environment for CartPole..
    env = gym.make('CartPole-v1')

    # Instantiate a policy network.
    policy = Policy(obs_size=obs_size, act_size=act_size, inner_size=inner_size)

    # Use the Adam optimizer.
    optimizer = torch.optim.Adam(params=policy.parameters(), lr=lr)

    # Run for a while.
    episodes = 10000
    total_reward = 0.0   # For tracking total reward per episode.
    average_rewards = [] # To collect average reward per episode.
    for step in range(episodes):
        # Run an episode, render ever 100 episodes to see how it's going.
        (states, actions, logprobs, discounted_rewards) = run_episode(env,
                                                                      policy,
                                                                      episode_len,
                                                                      gamma=1.0,
                                                                      render=step % 100 == 0)
        # Keep track of reward per episode.
        total_reward += discounted_rewards[0]
        if step % 100 == 0:
            average_rewards.append(total_reward / 100)
            if (total_reward / 100) == 500:
                print('Environment solved!')
                break
            
            print('Average reward @ episode {}: {}'.format(step, total_reward / 100))
            total_reward = 0.0
        
        # Run backpropagation for all steps of episode.
        optimizer.zero_grad()
        loss = -torch.sum(torch.tensor(discounted_rewards) * torch.stack(logprobs))
        loss.backward()
        optimizer.step()
        
    # Plot average reward per 100 episodes.
    plt.plot(average_rewards)
    plt.ylabel('Average reward')
    plt.xlabel('Episodes / 100')
    plt.show()

    # Animate some runs with the final trained agent.
    for step in range(100):
        run_episode(env, policy, episode_len, gamma=1.0, render=True)
